import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:async/async.dart';

class Settings extends StatefulWidget {

  Settings({
    Key key,
    this.title,
    this.onChangeTheme,
    this.onChangeImgResTap
  }) : super(key: key);

  final String title;
  final ValueChanged<bool> onChangeTheme;
  final ValueChanged<bool> onChangeImgResTap;

  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {

  bool _currentThemeIsDark;
  bool _currentImgPrefIsHi;
  SharedPreferences _prefs;
  final AsyncMemoizer _memoizer = AsyncMemoizer();

  // memoizer ensures FutureBuilder only loads preferences once, then caches into memory for smoother setting changes
  _loadPrefs() => _memoizer.runOnce(() async {
    _prefs = await SharedPreferences.getInstance();
    _currentThemeIsDark = _prefs.getBool('THEME_DARK') ?? false;
    _currentImgPrefIsHi = _prefs.getBool('IMG_RES_HI') ?? false;
  });

  _setPrefs() async {
    _prefs = await SharedPreferences.getInstance();
    _prefs.setBool('THEME_DARK', _currentThemeIsDark);
    _prefs.setBool('IMG_RES_HI', _currentImgPrefIsHi);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ListView(
        padding: EdgeInsets.fromLTRB(16.0, 8.0, 16.0, 8.0),
        children: <Widget>[
          _buildSwitchSetting('Dark Mode'),
          _buildSwitchSetting('High Resolution Images'),
        ],
      ),
    );
  }

  _buildSwitchSetting(String settingName) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        SizedBox(
            width: 300,
            child: Text(settingName)
        ),
        Container(
            width: 1.5,
            height: 25.0,
            color: Theme.of(context).backgroundColor
        ),
        Expanded(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              FutureBuilder(
                  future: _loadPrefs(),
                  builder: (context, snapshot) {
                    switch (snapshot.connectionState) {
                      case ConnectionState.active:
                      case ConnectionState.none:
                        return new Text('none');
                      case ConnectionState.waiting:
                        return new Text('waiting');
                      case ConnectionState.done:
                        switch(settingName) {
                          case 'Dark Mode':
                            ////////////////// TODO STATEFUL (SETSTATE)
                          ////////////////////////////////////////
                            return Switch(
                              value: _currentThemeIsDark,
                              onChanged: (value) {
                                widget.onChangeTheme(value);
                                setState(() {
                                  _currentThemeIsDark = value;
                                  _setPrefs();
                                });
                              },
                            );
                          case 'High Resolution Images':
                            return Switch(
                              value: _currentImgPrefIsHi,
                              onChanged: (value) {
                                widget.onChangeImgResTap(value);
                                setState(() {
                                  _currentImgPrefIsHi = value;
                                  _setPrefs();
                                });
                              },
                            );
                        }
                        ///////////////////////////////////////////
                    ///////////////////////////////
                    }
                  }
              )
            ],
          ),
        ),
      ],
    );
  }

}