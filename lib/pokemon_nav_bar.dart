import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'jigs/PokeCard.dart';

class PokemonNavBar extends StatelessWidget {
  final PokeType currentType;
  final ValueChanged<PokeType> onTypeTap;
  final List<PokeType> _types = PokeType.values;

  const PokemonNavBar({
    Key key,
    @required this.currentType,
    @required this.onTypeTap,
  })  : assert(currentType != null),
        assert(onTypeTap != null);

  Widget _buildType(PokeType type, BuildContext context) {
    final typeString =
    type.toString().replaceAll('PokeType.', '');
    return GestureDetector(
      onTap: () => onTypeTap(type),
      child: type == currentType
          ? Column(
              children: <Widget>[
                SizedBox(height: 16.0),
                Text(
                  typeString,
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 14.0),
                Container(
                  width: 70.0,
                  height: 2.0,
                  color: Colors.deepPurple,
                ),
              ],
            )
          : Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: 16.0,
                  vertical: 16.0,
              ),
              child: Text(
                typeString,
                textAlign: TextAlign.center,
              ),
           ),
    );
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> drawerList = List<Widget>();
    drawerList.add(
        Padding(
          padding: EdgeInsets.fromLTRB(40.0, 16.0, 16.0, 16.0),
          child: Text(
            'Filter by Type:',
            textScaleFactor: 1.5,
          ),
        )
    );
    drawerList.addAll(_types.map((PokeType t) => _buildType(t, context)).toList());
    return Drawer(
      child: Container(
        padding: EdgeInsets.only(top: 40.0),
        child: ListView(
            children: drawerList)
      ),
    );
  }
}