import 'package:flutter/material.dart';

import 'pokemon_loader.dart';
import 'jigs/PokeCard.dart';
import 'pokemon_nav_bar.dart';

// the activity layout/fragment equivalent
class HomePage extends StatefulWidget {

  HomePage({
    Key key,
    this.title,
    this.areHiRes,
    this.currentType,
    this.onTypeTap,
  }) : super(key: key);

  final String title;
  final bool areHiRes;
  final PokeType currentType;
  final ValueChanged<PokeType> onTypeTap;

  @override
  _HomePageState createState() => _HomePageState();
}

// the layout/fragment definition
class _HomePageState extends State<HomePage> {

  // the layout xml layout file equivalent
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.settings,
              semanticLabel: 'settings',
            ),
            onPressed: () {
              Navigator.of(context).pushNamed('/settings');
            },
          ),
        ],
      ),
      ///////////// TODO STATELESS WIDGET
      ///////////////////////////////////
      // https://medium.com/flutter-community/working-with-apis-in-flutter-8745968103e9
      body: PokemonLoader(
        type: widget.currentType,
        areHiRes: widget.areHiRes,
      ),
      ////////////
      ///////////////////////////////////
      drawer: PokemonNavBar(
          currentType: widget.currentType,
          onTypeTap: widget.onTypeTap,
      ),
    );
  }

}