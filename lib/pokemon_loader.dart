import 'package:flutter/material.dart';

import 'jigs/PokeCard.dart';
import 'jigs/CardList.dart';

import 'dart:convert';
import 'dart:async';
import 'dart:math';
import 'package:http/http.dart' as http;

class PokemonLoader extends StatelessWidget {

  PokemonLoader({
    Key key,
    this.type,
    this.areHiRes,
  }) : super(key: key);

  final PokeType type;
  final bool areHiRes;

  final List<Completer<PokeCard>> completers = List<Completer<PokeCard>>();
  final int pageSize = 20;
  final int total = 808;

  List<PokeCard> _cardsByType = List<PokeCard>();

  @override
  Widget build(BuildContext context) {
    if (type == PokeType.All) {
      return ListView.builder(
        itemCount: total,
        itemBuilder: (BuildContext context, int index) => _buildPokemonList(index),
      );
    }
    else {
      /////////////////// TODO ASYNC WIDGET BUILDING
      //////////////////////////////////////////////
      return FutureBuilder(
          future: _loadPokemonByType(type),
          builder: (context, snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.active:
              case ConnectionState.none:
                return new Text('none');
              case ConnectionState.waiting:
                return new Text('waiting');
              case ConnectionState.done:
                if (snapshot.hasData) {
                  _cardsByType = snapshot.data;
                  return ListView.builder(
                    itemCount: _cardsByType.length,
                    itemBuilder: (BuildContext context, int index) => _buildPokemonCard(_cardsByType[index]),
                  );
                } else if (snapshot.hasError) {
                  return new Text(
                    '${snapshot.error}',
                    style: TextStyle(color: Colors.red),
                  );
                }
                return Text('No data');
            }
          }
      );
    }
    /////////////////////////////////////////////////
    //////////////////////
  }

  /////////////////// TODO API CALLS (ASYNCHRONOUS)
  /////////////////////////////////////////////////
  Future<List<PokeCard>> _loadPokemonByType(PokeType lookupType) async {
    String dataURL = "https://api.pokemontcg.io/v1/cards?types=" + lookupType.toString().replaceAll('PokeType.', '');
    http.Response response = await http.get(dataURL);
    final cardMap = json.decode(response.body);

    CardList cardList = CardList.fromJson(cardMap);
    return cardList.getCards();
  }

  // Load pokemon by pokedex number
  Future<PokeCard> _loadPokemonByNationalNumber(int i) async {
    String dataURL = "https://api.pokemontcg.io/v1/cards?nationalPokedexNumber=" + i.toString();
    http.Response response = await http.get(dataURL);
    final cardMap = json.decode(response.body);

    CardList cardList = CardList.fromJson(cardMap);
    return cardList.getCards()[0];
  }
  ////////////////////////////////////////////////
  //////////////////////

  // https://flutter-academy.com/flutter-listview-infinite-scrolling/
  Future<List<Future<PokeCard>>> _retrieveAdditionalPokemon (int offset, int limit) {
    return Future.sync( () {
        return List.generate(limit, (index) {
          int pokedexNumber = offset + index + 1;
          return _loadPokemonByNationalNumber(pokedexNumber);
        });
    });
  }

  Widget _buildPokemonList(int itemIndex) {
    if (itemIndex >= completers.length) {
      int toLoad = min(total - itemIndex, pageSize);
      completers.addAll(List.generate(toLoad, (index) {
        return new Completer<PokeCard>();
      }));
      _retrieveAdditionalPokemon(itemIndex, toLoad).then((items) {
        items.asMap().forEach((index, item) {
          completers[itemIndex + index].complete(item);
        });
      }).catchError((error) {
        completers.sublist(itemIndex, itemIndex + toLoad).forEach((completer) {
          completer.completeError(error);
        });
      });
    }

    var future = completers[itemIndex].future;
    return new FutureBuilder(
        future: future,
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return CircularProgressIndicator();
            case ConnectionState.done:
              if (snapshot.hasData) {
                return _buildPokemonCard(snapshot.data);
              } else if (snapshot.hasError) {
                return new Text(
                  '${snapshot.error}',
                  style: TextStyle(color: Colors.red),
                );
              }
              return Text('No data');
            default:
              return new Text('Default');
          }
        });
  }

  // Build formatted card
  Widget _buildPokemonCard(PokeCard card) {
    return Container(
        height: 300.0,
        child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,

            children: <Widget>[
              Padding(
                padding: EdgeInsets.fromLTRB(2.0, 16.0, 16.0, 2.0),
                child: AspectRatio(
                  aspectRatio: 13 / 15,
                  child: Image.network(
                    (areHiRes) ? card.imageUrlHiRes : card.imageUrl,
                    fit: BoxFit.contain,
                  ),
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    child: Text(
                      card.getName(),
                      textScaleFactor: (card.getName().length > 14) ? 1.0 : 1.5,
                      textAlign: TextAlign.start,
                      softWrap: true,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                    ),
                  ),
                ],
              ),
            ]
        )
    );
  }

}