import 'package:flutter/material.dart';

import 'home.dart';
import 'settings.dart';
import 'jigs/PokeCard.dart';
import 'package:shared_preferences/shared_preferences.dart';

// https://flutter.dev/docs/get-started/flutter-for/android-devs
// https://pub.dev/packages/sqflite
void main() {
  runApp(MyApp());
}

// https://flutter.dev/docs/development/ui/widgets/layout
class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

// the MainActivity equivalent, XML layout included
class _MyAppState extends State<MyApp> {

  PokeType _currentType = PokeType.All;
  ThemeData _currentTheme = ThemeData.dark();
  bool _areHiRes = false;

  // onCreate() equivalent
  @override
  void initState() {
    super.initState();
    _loadPrefs();
  }

  ///////////////// TODO SHARED PREFERENCES
  /////////////////////////////////////////
  _loadPrefs() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      if (prefs.containsKey('THEME_DARK')) {
        _currentTheme = prefs.getBool('THEME_DARK') ? ThemeData.dark() : ThemeData.light();
      }
      else {
        prefs.setBool('THEME_DARK', true);
      }
      if (prefs.containsKey('IMG_RES_HI')) {
        _areHiRes = prefs.getBool('IMG_RES_HI');
      }
      else {
        prefs.setBool('IMG_RES_HI', false);
      }
    });
  }
  ///////////////////////////////////////
  //////////////////

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: _currentTheme,
      home: HomePage(
        title: 'Home Page',
        areHiRes: _areHiRes,
        currentType: _currentType,
        onTypeTap: _onTypeTap,
      ),
      ////////////////// TODO NAVIGATION
      //////////////////////////////
      routes: {
        '/settings': (context) => Settings(
          title: 'Settings',
          onChangeTheme: _onChangeTheme,
          onChangeImgResTap: _onChangeImgResTap,
        ),
      },
      ////////////////////////

    );
  }

  void _onTypeTap(PokeType type) {
    setState(() {
      _currentType = type;
    });
  }

  /////////////// TODO CALLBACK
  ////////////////////////
  /// Change the theme from light to dark
  void _onChangeTheme(bool switchValue) {
    setState(() {
      _currentTheme = switchValue ? ThemeData.dark() : ThemeData.light();
    });
  }

  // Change the image resolution from hi to low
  void _onChangeImgResTap(bool switchValue) {
    setState(() {
      _areHiRes = switchValue;
    });
  }
///////////////////////////
/////////////////

}
