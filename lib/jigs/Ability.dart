
class Ability {

  String name;
  String text;
  String type;

  Ability({this.name, this.text, this.type});

  factory Ability.fromJson(Map<String, dynamic> json) {

    return new Ability(
      name: json['name'],
      text: json['text'],
      type: json['type']
    );
  }
}