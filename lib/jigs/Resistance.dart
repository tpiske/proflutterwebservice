
class Resistance {

  String type;
  String value;

  Resistance({this.type, this.value});

  factory Resistance.fromJson(Map<String, dynamic> json) {

    return new Resistance(
      type: json['type'],
      value: json['value']
    );
  }
}