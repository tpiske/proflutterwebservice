
class Attack {

  List<String> cost;
  String name;
  String text;
  String damage;
  int convertedEnergyCost;

  Attack(this.cost, this.name, this.text, this.damage, this.convertedEnergyCost);

  // https://medium.com/flutter-community/parsing-complex-json-in-flutter-747c46655f51
  factory Attack.fromJson(Map<String, dynamic> json) {
    var costDynamic  = json['cost'];
    List<String> cost = costDynamic.cast<String>();

    String damage_ = json['damage'].toString();

    return new Attack(
      cost,
      json['name'],
      json['text'],
      damage_,
      json['convertedEnergyCost']);
  }
}