import 'PokeCard.dart';

class CardList {
  final List<PokeCard> cards;

  CardList({this.cards});

  factory CardList.fromJson(Map<String, dynamic> json) {
    List<PokeCard> cards = new List<PokeCard>();
    List<dynamic> jsonCards = json['cards'];
    cards = jsonCards.map((i) => PokeCard.fromJson(i)).toList();

    return new CardList(
      cards: cards,
    );
  }

  getCards() {
    return cards;
  }
}