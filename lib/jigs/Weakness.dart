
class Weakness {

  String type;
  String value;

  Weakness({this.type, this.value});

  factory Weakness.fromJson(Map<String, dynamic> json) {

    return new Weakness(
        type: json['type'],
        value: json['value']
    );
  }
}