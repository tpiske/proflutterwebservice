import 'Ability.dart';
import 'Attack.dart';
import 'Resistance.dart';
import 'Weakness.dart';

enum PokeType { All, Colorless, Darkness, Dragon, Fairy, Fighting,
Fire, Grass, Lightning, Metal, Psychic, Water }

class PokeCard {

  String id;
  String name;
  int nationalPokedexNumber;
  String imageUrl;
  String imageUrlHiRes;
  List<String> types;
  String supertype;
  String subtype;
  String evolvesFrom;
  String hp;
  List<String> retreatCost;
  int convertedRetreatCost;
  String number;
  String artist;
  String rarity;
  String series;
  String set;
  String setCode;
  List<Attack> attacks;
  List<Resistance> resistances;
  List<Weakness> weaknesses;
  //Ability ability;

  PokeCard({this.id, this.name, this.nationalPokedexNumber, this.imageUrl, this.imageUrlHiRes,
      this.types, this.supertype, this.subtype, this.evolvesFrom, this.hp, this.retreatCost,
      this.convertedRetreatCost, this.number, this.artist, this.rarity, this.series,this.set,
      this.setCode, this.attacks, this.resistances, this.weaknesses}); //, this.ability});

  // https://medium.com/flutter-community/parsing-complex-json-in-flutter-747c46655f51
  factory PokeCard.fromJson(Map<String, dynamic> json) {
    try {
      var typesDynamic = json['types'];
      List<String> types = List<String>();
      if (typesDynamic != null) {
        types = typesDynamic.cast<String>();
      }

      var retreatCostDynamic = json['retreatCost'];
      List<String> retreatCost = List<String>();
      if (retreatCostDynamic != null) {
        retreatCost = retreatCostDynamic.cast<String>();
      }

      var attacksList = json['attacks'] as List;
      List<Attack> attacks = List<Attack>();
      if (attacksList != null) {
        attacks = attacksList.map((i) => Attack.fromJson(i)).toList();
      }

      var resistancesList = json['resistances'] as List;
      List<Resistance> resistances = new List<Resistance>();
      if (resistancesList != null) {
        resistances =
            resistancesList.map((i) => Resistance.fromJson(i)).toList();
      }

      var weaknessesList = json['weaknesses'] as List;
      List<Weakness> weaknesses = List<Weakness>();
      if (weaknessesList != null) {
        weaknesses = weaknessesList.map((i) => Weakness.fromJson(i)).toList();
      }

      ////////////////// TODO JSON PARSING
      ////////////////////////////////////
      return new PokeCard(
          id: json['id'],
          name: json['name'],
          nationalPokedexNumber: json['nationalPokedexNumber'],
          imageUrl: json['imageUrl'],
          imageUrlHiRes: json['imageUrlHiRes'],
          types: types,
          supertype: json['supertype'],
          subtype: json['subtype'],
          evolvesFrom: json['evolvesFrom'],
          hp: json['hp'].toString(),
          retreatCost: retreatCost,
          convertedRetreatCost: json['convertedRetreatCost'],
          number: json['number'],
          artist: json['artist'],
          rarity: json['rarity'],
          series: json['series'],
          set: json['set'],
          setCode: json['setCode'],
          attacks: attacks,
          resistances: resistances,
          weaknesses: weaknesses);
      //ability: Ability.fromJson(json['ability']));
    }
    //////////////////////////////////////
    /////////////////////
    catch(e) {
      print('problem loading ' + json['name'] + " with id " + json['id']);
      return new PokeCard(
          id: "",
          name: "",
          nationalPokedexNumber: 0,
          imageUrl: "https://cdn.bulbagarden.net/upload/1/17/Cardback.jpg",
          imageUrlHiRes: "https://cdn.bulbagarden.net/upload/1/17/Cardback.jpg",
          types: null,
          supertype: "",
          subtype: "",
          evolvesFrom: "",
          hp: "",
          retreatCost: null,
          convertedRetreatCost: 0,
          number: "",
          artist: "",
          rarity: "",
          series: "",
          set: "",
          setCode: "",
          attacks: null,
          resistances: null,
          weaknesses: null);
    }
  }

  getName() {
    return name;
  }
}