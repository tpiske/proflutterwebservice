import 'package:sqflite/sqflite.dart';

final String tableCards = 'cards';
final String columnId = '_id';
final String columnName = 'name';
final String columnPicUrl = 'picUrl';

class SavedCard {
  int id;
  String name;
  String picUrl;

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      columnName: name,
      columnPicUrl: picUrl
    };
    if (id != null) {
      map[columnId] = id;
    }
    return map;
  }

  SavedCard();

  SavedCard.fromMap(Map<String, dynamic> map) {
    id = map[columnId];
    name = map[columnName];
    picUrl = map[columnPicUrl];
  }
}

class SavedCardProvider {
  Database db;

  Future open(String path) async {
    db = await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
          await db.execute('''
create table $tableCards ( 
  $columnId integer primary key autoincrement, 
  $columnName text not null,
  $columnPicUrl text not null)
''');
        });
  }

  Future<SavedCard> insert(SavedCard card) async {
    card.id = await db.insert(tableCards, card.toMap());
    return card;
  }

  Future<SavedCard> getCard(int id) async {
    List<Map> maps = await db.query(tableCards,
        columns: [columnId, columnPicUrl, columnName],
        where: '$columnId = ?',
        whereArgs: [id]);
    if (maps.length > 0) {
      return SavedCard.fromMap(maps.first);
    }
    return null;
  }

  Future<int> delete(int id) async {
    return await db.delete(tableCards, where: '$columnId = ?', whereArgs: [id]);
  }

  Future<int> update(SavedCard card) async {
    return await db.update(tableCards, card.toMap(),
        where: '$columnId = ?', whereArgs: [card.id]);
  }

  Future close() async => db.close();
}